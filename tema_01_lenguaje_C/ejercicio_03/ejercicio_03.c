#include<stdio.h>


float polinomio(int lambda)
{
    return lambda*lambda + lambda + 1;
}


float normalizar(float x, float xmin, float xmax, float a, float b)
{
    float xnor = 0.0;

    xnor = a + (x - xmin)*(b - a)/(xmax - xmin);

    return xnor;
}

int main(void)
{

    int i = 0;

    int intervalo;

    float a = 0.0;
    float b = 0.0;

    float x = 0.0;
    float xmin = 3;
    float xmax = 421;

    float xnor = 0.0;


    printf("Eliga el intervalo en que desea normalizar los valores de la expresión lambda^2 + lambda + 1\n");
    printf("Digite 1 para elegir el interválo: [-1, 1].\n");
    printf("Digite 2 para elegir el interválo: [1, 10].\n");
    printf("Digite 3 para elegir el interválo: [0.5, 1].\n");
    scanf("%d", &intervalo);

    printf("\n");

    switch(intervalo)
    {
        case 1:
            a = -1;
            b = 1;
            break;
        case 2:
            a = 1;
            b = 10;
            break;
        case 3:
            a = 0.5;
            b = 1;
            break;
    }

    printf("Cuando el intervalo de la normalizarión es [%0.2f, %0.2f], los valores son:\n\n",a, b);

    for(i=1; i<=20; i++)
    {
        x = polinomio(i);
        xnor = normalizar(x, xmin, xmax, a, b);

        printf("x = %f   y   xnor = %f\n", x, xnor);
    }

    return 0;
}
