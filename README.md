# Material de apoyo para Microprocesamiento I

## Instalación de las herramientas en Debian:

#### Instalar el editor nano
sudo apt-get install -y nano


#### Instalar compilador gcc
sudo apt-get install -y gcc

>>>
##### Compilar un programa 
gcc -Wall -o ejecutar nombre_programa.c

##### Ejecutar un programa
./ejecutar
>>>


#### Instalar multiplexor de terminal
sudo apt-get install -y tmux

